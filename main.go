package main

import (
	"blogpost/models"
	"blogpost/routers"
	"fmt"
	"net/http"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {

	//router := gin.Default()
	router := routers.RegisterRoutes()
	// serve frontend static files
	router.Use(static.Serve("/", static.LocalFile("./views", true)))

	//router := routers.RegisterRoutes()
	models.Init()

	// setup route group for the API
	api := router.Group("/api")
	{
		api.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"message": "pong",
			})
		})

		//React
		api.GET("/home", func(ctx *gin.Context) {
			fmt.Println("under home")
			ctx.Header("Content-Type", "application/json")
			ctx.Redirect(http.StatusMovedPermanently, "http://localhost:3000/")
		})
	}

	fmt.Printf("\nSuccessfully connected to database!\n")

	router.Run("localhost:8080")

}
