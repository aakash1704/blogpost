package routers

import (
	"net/http"

	"blogpost/models"

	"blogpost/jwtAuth"

	"github.com/gin-gonic/gin"
)

type blog struct {
	ID      string `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

//var DB = models.Init()

func RegisterRoutes() *gin.Engine {
	router := gin.Default()

	//routes
	router.GET("/content", getTasks)
	router.GET("/content/:id", getTaskById)
	router.POST("/content", postTasks)
	router.DELETE("/content/:id", deleteTask)
	router.POST("/signup", jwtAuth.SignUp)
	// signup
	//router.PUT("/task/:id", updateTask)

	return router
}

//get all content

func getTasks(c *gin.Context) {
	var allblog []models.Blog
	models.DB.Find(&allblog)

	c.IndentedJSON(http.StatusOK, gin.H{"data": allblog})
}

func getTaskById(c *gin.Context) {
	var blog models.Blog
	if err := models.DB.Where("id = ?", c.Param("id")).First(&blog).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": blog})
}

// post a new task
func postTasks(c *gin.Context) {
	var newtodo blog

	if err := c.ShouldBindJSON(&newtodo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tod := models.Blog{Title: &newtodo.Title, Content: &newtodo.Content}
	models.DB.Create(&tod)
	c.IndentedJSON(http.StatusCreated, gin.H{"data": tod})

}

// delete a task
func deleteTask(c *gin.Context) {
	var delTodo models.Blog
	if err := models.DB.Where("id = ?", c.Param("id")).First(&delTodo).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&delTodo)

	c.JSON(http.StatusOK, gin.H{"data": true})

}

/*
func RegisterRoutes() *gin.Engine {
	router := gin.Default()

	// routes
	router.GET("/blog", GetTasks)
	//router.GET("/task/:id", GetTaskById)
	router.POST("/blog", PostTasks)
	// router.POST("/signup", jwtAuth.SignUp)
	router.DELETE("/blog/:id", DeleteTask)
	//router.PUT("/task/:id", UpdateTask)

	return router
}

// get all tasks
func GetTasks(c *gin.Context) {
	var allBlog []models.Blog
	models.DB.Find(&allBlog)

	c.IndentedJSON(http.StatusOK, gin.H{"data": allBlog})
}

// post a new task
func PostTasks(c *gin.Context) {
	var newtodo CreateTaskInput

	if err := c.ShouldBindJSON(&newtodo); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	tod := models.Blog{Author: &newtodo.Author, Title: &newtodo.Title, Content: &newtodo.Content}
	models.DB.Create(&tod)
	c.IndentedJSON(http.StatusCreated, gin.H{"data": tod})
}

// delete a task
func DeleteTask(c *gin.Context) {
	var delTodo models.Blog
	if err := models.DB.Where("id = ?", c.Param("id")).First(&delTodo).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&delTodo)

	c.JSON(http.StatusOK, gin.H{"data": true})

}

*/

/*
func getTasks(c *gin.Context) {
	var allTask []blog
	out, err := DB.Query(`SELECT * from db_blogpost`)
	if err != nil {
		panic(err)
	}
	for out.Next() {
		var id string
		var title string
		var content string
		err = out.Scan(&id, &title, &content)
		if err != nil {
			panic(err)
		}
		allTask = append(allTask, blog{ID: id, Title: title, Content: content})

	}
	c.IndentedJSON(http.StatusOK, allTask)
}

func postTasks(c *gin.Context) {
	var newtodo blog

	if err := c.BindJSON(&newtodo); err != nil {
		return
	}

	// todos = append(todos, newtodo) */
//sqlStmnt := `INSERT INTO db_blogpost (id,title,content) VALUES ($1,$2,$3)`
//_, err := DB.Exec(sqlStmnt, newtodo.ID, newtodo.Title, newtodo.Content)
//if err != nil {
//	panic(err)
//}
//c.IndentedJSON(http.StatusCreated, newtodo)
//}
/*
func getTaskById(c *gin.Context) {
	id := c.Param("id")
	var allTask []blog
	sqlStmnt := `SELECT * from db_blogpost WHERE ID IN ($1)`
	out, err := DB.Query(sqlStmnt, id)
	if err != nil {
		panic(err)
	}
	for out.Next() {
		var id string
		var title string
		var content string
		err = out.Scan(&id, &title, &content)
		if err != nil {
			panic(err)
		}
		allTask = append(allTask, blog{ID: id, Title: title, Content: content})

	}
	c.IndentedJSON(http.StatusOK, allTask)
	/*for _, a := range todos {
	  if a.ID == id {
	    c.IndentedJSON(http.StatusOK, a)
	    return
	  }
	}*/
//c.IndentedJSON(http.StatusNotFound, gin.H{"message": "task not found"})
//}
/*
func updateTask(c *gin.Context) {
	id := c.Param("id")
	var update blog

	if err := c.BindJSON(&update); err != nil {
		return
	}

	sqlStmnt := `UPDATE db_blogpost SET title=$1, content=$2 WHERE id=$3`
	_, err := DB.Exec(sqlStmnt, update.Title, update.Content, id)
	if err != nil {
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"message": "updated successfully"})
}

func deleteTask(c *gin.Context) {
	id := c.Param("id")
	sqlStatement := `DELETE FROM db_blogpost WHERE id = $1;`
	_, err := DB.Exec(sqlStatement, id)
	if err != nil {
		panic(err)
	}
	c.IndentedJSON(http.StatusOK, gin.H{"message": "deleted"})
}
*/
