package models

import (
	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Blog struct {
	ID      uint    `gorm:"primary key:autoIncrement" json:"id"`
	Title   *string `json:"title"`
	Content *string `json:"content"`
}

var DB *gorm.DB

func Init() {
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: "host=localhost user=postgres dbname=blogpost password=Aaka$h1234 sslmode=disable",
	}))
	if err != nil {
		panic("Error:Failed to connect to database!")
	}

	db.AutoMigrate(&Blog{})

	DB = db
}

// package storage

// import (
// 	"fmt"

// 	"gorm.io/driver/postgres"
// 	"gorm.io/gorm"
// )

// type Config struct {
// 	Host     string
// 	Port     string
// 	Password string
// 	User     string
// 	DBName   string
// 	SSLMode  string
// }

// func NewConnection(config *Config) (*gorm.DB, error) {
// 	dsn := fmt.Sprintf(
// 		"host=%s port=%s user=%s  password=%s dbname=%s sslmode=diable",
// 		config.Host, config.Port, config.User, config.Password, config.DBName,
// 	)
// 	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
// 	if err != nil {
// 		return db, err
// 	}
// 	return db, nil

// }

// connection
// package models

// import (
// 	_ "github.com/lib/pq"
// 	"gorm.io/driver/postgres"
// 	"gorm.io/gorm"
// )

// type Blog struct {
// 	ID      uint    `gorm:"primary key:autoIncrement" json:"id"`
// 	Author  *string `json:"author"`
// 	Title   *string `json:"title"`
// 	Content *string `json:"content"`
// }

// var DB *gorm.DB

// func ConnectDatabase() {
// 	db, err := gorm.Open(postgres.New(postgres.Config{
// 		DSN: "host=localhost user=postgres dbname=blogApp password=admin sslmode=disable",
// 	}))
// 	if err != nil {
// 		panic("Error:Failed to connect to database!")
// 	}

// 	db.AutoMigrate(&Blog{})

// 	DB = db
// }

// package models

// import (
// 	//"database/sql"

// 	_ "github.com/lib/pq"

// 	"gorm.io/driver/postgres"
// 	"gorm.io/gorm"
// )

// var db *gorm.DB

// type DB struct {
// 	*sql.DB
// }

// func Init() *sql.DB {
// 	var err error
// 	connStr := "user=postgres dbname=blogpost password=Aaka$h1234 host=localhost sslmode=disable port=5432"
// 	db, err = sql.Open("postgres", connStr)

// 	if err != nil {
// 		panic(err)
// 	}
// 	//defer db.Close()

// 	err = db.Ping()
// 	if err != nil {
// 		panic(err)
// 	}
// 	return db
// }
